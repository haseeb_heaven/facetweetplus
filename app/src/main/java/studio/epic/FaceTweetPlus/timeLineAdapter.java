package studio.epic.FaceTweetPlus;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class timeLineAdapter extends BaseAdapter {

    ArrayList<String> statusArray = new ArrayList<>();
    ArrayList<String> likesArray = new ArrayList<>();
    ArrayList<String> commentsArray = new ArrayList<>();
    Activity mContext;
    TextView statusTxt, likesTxt,commentsTxt;

    public timeLineAdapter(Activity mainActivity, ArrayList<String> statusList, ArrayList<String> likesList, ArrayList<String> commentsList) {
        mContext = mainActivity;
        statusArray = statusList;
        likesArray = likesList;
        commentsArray = commentsList;
    }


    @Override
    public int getCount() {
        return statusArray.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        Log.d("ADPTR", "statusArray = " + statusArray);
        Log.d("ADPTR", "statusArray.size = " + statusArray.size());

        Log.d("ADPTR", "likesArray = " + likesArray);
        Log.d("ADPTR", "likesArray.size = " + likesArray.size());

        Log.d("ADPTR", "commentsArray = " + commentsArray);
        Log.d("ADPTR", "commentsArray.size() = " + commentsArray.size());

        if (view == null) {
            LayoutInflater lf = mContext.getLayoutInflater();
            view = lf.inflate(R.layout.timeline_layout, parent, false);
        }

        //Finding UI elements in layout.
        statusTxt = (TextView) view.findViewById(R.id.timeLineStatus);
        likesTxt = (TextView) view.findViewById(R.id.timeLineLikes);
        commentsTxt = (TextView) view.findViewById(R.id.timeLineComments);

        //Setting fetched data items to ListView elements.
        try {

            statusTxt.setText(statusArray.get(position));
            likesTxt.setText(likesArray.get(position));
            commentsTxt.setText(commentsArray.get(position));
        }catch (IndexOutOfBoundsException ex){
            Log.d("ADPTR","in ListView Adapter : "+ex);
        }

        return view;
    }
}

