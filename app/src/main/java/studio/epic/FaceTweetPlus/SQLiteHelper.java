package studio.epic.FaceTweetPlus;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;


public class SQLiteHelper extends SQLiteOpenHelper {
    Context c;

    public static enum changePassEnum {RESULT_SUCEESS, RESULT_NOT_INSERTED, RESULT_AUTHFAIL}

    ;
    private static String emailFromDB = null;
    protected static String currentUserEmail = null;
    protected static String currentUserName = null;

    String create_table = "create table if not exists register(name varchar(20),email varchar(20),pass varchar(20),gender varchar,phone varchar(10),hasDP varchar(10))";


    public SQLiteHelper(Context context) {
        super(context, "myDataBase", null, 1);
        c = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Stores all the user infromation in the register table....
    public boolean insetToDB(String name, String pass, String email, String gender, String phone, String hasDP) {
        ContentValues cv = new ContentValues();

        cv.put("name", name);
        cv.put("pass", pass);
        cv.put("email", email);
        cv.put("phone", phone);
        cv.put("gender", gender);
        cv.put("hasDP", hasDP);

        SQLiteDatabase sdb = openDB();
        long user_inserted = sdb.insert("register", null, cv);

        if (user_inserted >= 1) {
            closeDB(sdb);
            return true;
        }

        closeDB(sdb);
        return false;
    }


    //Authenticates the user from the database
    public boolean authenticate(String email, String pass) {
        boolean auth = false;
        String col[] = {"email", "pass"};
        String sel_args[] = {email, pass};

        emailFromDB = null;
        String passFromDB = null;

        SQLiteDatabase db = openDB();
        Cursor cursor = db.query("register", col, "email = ? and pass = ?", sel_args, null, null, null);

        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                int emailIndex = cursor.getColumnIndex("email");
                emailFromDB = cursor.getString(emailIndex);
                int passIndex = cursor.getColumnIndex("pass");
                passFromDB = cursor.getString(passIndex);
            }

            if (email.equals(emailFromDB) && pass.equals(passFromDB)) {
                closeDB(db);
                cursor.close();
                currentUserAndEmail();
                auth = true;
            } else {
                closeDB(db);
                cursor.close();
                auth = false;
            }

        } else {
            closeDB(db);
            cursor.close();
            auth = false;
        }

        return auth;
    }

    //return Current User and Email from Database...
    protected ArrayList<String> currentUserAndEmail() {

        currentUserName = getUserFromEmail(emailFromDB);
        currentUserEmail = emailFromDB;

        ArrayList<String> mArrayList = new ArrayList<>();
        mArrayList.add(0, currentUserEmail);
        mArrayList.add(1, currentUserName);

        return mArrayList;
    }


    public boolean userHaveDP(String email) {

        String col[] = {"hasDP",};
        String sel_args[] = {email};

        String dpFromDB = null;
        int dpIndex = 0;
        SQLiteDatabase db = openDB();
        Cursor cursor = db.query("register", col, "email = ?", sel_args, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                dpIndex = cursor.getColumnIndex("hasDP");
                dpFromDB = cursor.getString(dpIndex);
            }
        }

        Log.d("aaa", "in SQLite dpindex = " + dpIndex);
        Log.d("aaa", "in SQLite dpfromDB = " + dpFromDB);

        try {

            if (dpFromDB.equals("true")) {
                closeDB(db);
                cursor.close();
                return true;
            } else {
                closeDB(db);
                cursor.close();
                return false;
            }
        } catch (NullPointerException ex) {
            Log.d("aaa", "" + ex);
        }

        closeDB(db);
        return false;
    }

    //for Forgot Password....
    public String authRegEmail(String regEmail) {
        String col[] = {"pass"};
        String sel_args[] = {regEmail};

        String passFromDB = null;

        SQLiteDatabase db = openDB();
        Cursor cursor = db.query("register", col, "email = ?", sel_args, null, null, null);

        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                int passIndex = cursor.getColumnIndex("pass");
                passFromDB = cursor.getString(passIndex);
            }
            closeDB(db);
            cursor.close();
            return passFromDB;

        } else {
            closeDB(db);
            cursor.close();
            return null;
        }
    }


    //for Changing the user current password.
    public changePassEnum changeCurrentPass(String email, String oldPass, String newPass) {


        //checking if user exists or not.
        if (authenticate(email, oldPass)) {
            Log.d("qqq", "in changeCurrentPass authenticate");

            SQLiteDatabase db = openDB();

            //Content values stores the value im key-pair
            ContentValues cv = new ContentValues();
            cv.put("pass", newPass);

            String args[] = {email};
            int result = db.update("register", cv, "email = ?", args);
            closeDB(db);

            if (result > 0) {
                Log.d("qqq", "changePassEnum.RESULT_SUCEESS; " + changePassEnum.RESULT_SUCEESS);
                return changePassEnum.RESULT_SUCEESS;
            } else {
                Log.d("qqq", "changePassEnum.RESULT_NOT_INSERTED; " + changePassEnum.RESULT_NOT_INSERTED);
                return changePassEnum.RESULT_NOT_INSERTED;
            }
        } else {
            Log.d("qqq", "changePassEnum.RESULT_AUTHFAIL; " + changePassEnum.RESULT_AUTHFAIL);
            return changePassEnum.RESULT_AUTHFAIL;
        }

    }


    //method to recive user and email from Databse
    public String getUserFromEmail(String email) {

        String col[] = {"name"};
        String sel_args[] = {email};

        String nameFromDB = null;

        SQLiteDatabase db = openDB();
        Cursor cursor = db.query("register", col, "email = ?", sel_args, null, null, null);

        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                int nameIndex = cursor.getColumnIndex("name");
                nameFromDB = cursor.getString(nameIndex);
            }
            closeDB(db);
            cursor.close();
            return nameFromDB;

        } else {
            closeDB(db);
            cursor.close();
            return null;
        }
    }

    //generate UID for every user to distinguish DP
    protected String generateUID(String userName) {
        return userName + "_profile.jpg";
    }


    public SQLiteDatabase openDB() {
        return getWritableDatabase();
    }

    public void closeDB(SQLiteDatabase db) {
        db.close();
    }
}
