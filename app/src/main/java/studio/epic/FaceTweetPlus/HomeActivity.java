package studio.epic.FaceTweetPlus;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.camera2.params.Face;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQ_WHATSAPP = 5;
    private static final int FB_FRAG_ID = 1;
    private static final int WHTSAPP_FRAG_ID = 2;
    private static final int GPLUS_FRAG_ID = 3;
    static String masterStatus;
    EditText masterStatusTxt;
    CheckBox FaceBookCheckBox, GooglePlusCheckBox, WhatsAppCheckBox;
    private Toolbar myToolBar;
    private DrawerLayout myDrawer;
    private NavigationView myNavigation;
    public static CircleImageView mCircleProfile;
    private static TextView profUser, profEmail;
    public RegisterPage mRegister;
    private GooglePlusFragment gPlusFrag;
    FaceBookFragment fbFrag;
    public static LoginPage mLoginPage;
    protected static int onStartCalled = 0;
    protected static String imgPathFromPref;
    FragmentManager myFragmentManager;
    FragmentTransaction myFragTransaction;


    protected static boolean homeIsCalled = false;
    private SQLiteHelper mSQLiteHelper;
    private Menu mMenu;
    protected static boolean userSignedOut = false;
    private ProgressDialog progressDialog;
    protected static boolean gPlusCheckBoxChecked = false;
    private static boolean onDestroyCalled = false;
    protected static int userProfileSize = 70;

    public HomeActivity() {
        masterStatus = null;
        mRegister = new RegisterPage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //finding UI Elements.
        FaceBookCheckBox = (CheckBox) findViewById(R.id.fbCheckBox);
        GooglePlusCheckBox = (CheckBox) findViewById(R.id.gPlusCheckBox);
        WhatsAppCheckBox = (CheckBox) findViewById(R.id.whatsAppCheckBox);

        myDrawer = (DrawerLayout) findViewById(R.id.drawer);
        myToolBar = (Toolbar) findViewById(R.id.toolBar);
        myNavigation = (NavigationView) findViewById(R.id.navigationView);
        masterStatusTxt = (EditText) findViewById(R.id.MasterStatus);

        //for progress dialog
        progressDialog = new ProgressDialog(this);

        //Creating Object of LoginPage for Displaying SignOut Menu.!
        mLoginPage = new LoginPage();
        mRegister = new RegisterPage();
        gPlusFrag = new GooglePlusFragment();
        fbFrag = new FaceBookFragment(HomeActivity.this);

        mSQLiteHelper = new SQLiteHelper(this);

        //create The Facebook Fragment
        createFragment(FB_FRAG_ID);

        //Set Home Page back because it was replaced by last Fragment that was created.
        myFragmentManager.popBackStack();

        //Setting Header for Navigation and finding Header UI elements.
        View headerView = myNavigation.inflateHeaderView(R.layout.profile_layout);

        mCircleProfile = (CircleImageView) headerView.findViewById(R.id.profile_image);
        profUser = (TextView) headerView.findViewById(R.id.profileUserName);
        profEmail = (TextView) headerView.findViewById(R.id.profileEmail);

        //Setting Menu for Navigation
        myNavigation.inflateMenu(R.menu.navigation_menu);

        //Set ToolBar and set Button for Navigation View
        setSupportActionBar(myToolBar);
        ActionBarDrawerToggle actionBar = new ActionBarDrawerToggle(this, myDrawer, myToolBar, R.string.open, R.string.close);
        myDrawer.addDrawerListener(actionBar);
        actionBar.syncState();

        //Make Navigation clickable
        myNavigation.setNavigationItemSelectedListener(this);

        homeIsCalled = true;
        Log.d("zzz", "in onCreate Home onStartCalled = " + onStartCalled);

        //Logout from FB if it was not Logged out on App terminate
        FacebookSdk.sdkInitialize(this);
        Log.d("FB","in Oncreate Home isLogged in = "+fbFrag.isLoggedIn());
            fbFrag.logOutFB();
            //Toast.makeText(this, "LoggedOut from Facebook for to security reasons", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStart() {
        super.onStart();

        //Log.d("TAG","onStart Home fbFrag.logOutFB() = "+fbFrag.logOutFB());
        Log.d("TAG","onStart Home onDestroyCalled = "+onDestroyCalled);

        if (onDestroyCalled) {

        }


        String currUserEmail = mSQLiteHelper.currentUserEmail;
        String currUserName = mSQLiteHelper.currentUserName;
        boolean userHasDP = false;

        if (onStartCalled == 0) {
            Log.d("zzz", "in OnStart Home onStartCalled = " + onStartCalled);

            try {
                userHasDP = mSQLiteHelper.userHaveDP(currUserEmail);
            } catch (IllegalArgumentException ex) {
                Log.d("aaa", "" + ex);
            }


            Log.d("aaa", "in OnStart Home userSingedOut = " + userSignedOut);

            //for loading profile picture from storage
            if (userHasDP) {
                imgPathFromPref = getSharedPreferences(mRegister.PREFS_NAME, MODE_PRIVATE)
                        .getString(mRegister.PROFILE_PATH, "null");

                Toast.makeText(HomeActivity.this, "Profile Picture found",Toast.LENGTH_LONG).show();

                loadImageFromStorage(imgPathFromPref, currUserName);

                Log.d("abc", "in Homme Start genUID currUser = " + mSQLiteHelper.generateUID(currUserName));
                Log.d("abc", "in Home imgPathFromPref = " + imgPathFromPref);
            } else {
                Toast.makeText(HomeActivity.this, "You dont have any DP", Toast.LENGTH_SHORT).show();
                mCircleProfile.setImageResource(R.drawable.demo_user);
            }

            //Setting Email from Database
            if (currUserEmail != null && currUserName != null) {
                profEmail.setText(currUserEmail);
                profUser.setText(currUserName);
            }

        }


        onStartCalled++;
        if (onStartCalled > 1)
            onStartCalled = 1;

        Log.d("zzz", "in OnStart++ Home onStartCalled++ = " + onStartCalled);
    }

    //For Loading Profile Picture.
    protected void loadImageFromStorage(final String path, final String UID_profile) {

        //setting progress dialog
        progressDialog.setTitle("Profile!");
        progressDialog.setMessage("Loading Profile Picture Please Wait...");
        progressDialog.show();

        //creating seperate thread for Loading of image
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        //start of loading the image in seperate thread
                        try {
                            File mFile = new File(path, mSQLiteHelper.generateUID(UID_profile));
                            mRegister.bitmap = decodeFile(mFile);
                        } catch (OutOfMemoryError er) {
                            Toast.makeText(HomeActivity.this, "Memory Error = " + er, Toast.LENGTH_LONG).show();
                        }


                        //Set Image on UI Thread
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mCircleProfile.setImageBitmap(mRegister.bitmap);
                                progressDialog.setMessage("Image loaded successfully");
                                progressDialog.dismiss();
                            }
                        });
                    }


                }, 3000);

    }

    // Decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = userProfileSize;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (FileNotFoundException e) {
            Toast.makeText(HomeActivity.this, "Profile Picture Corrupted or Deleted from storage!", Toast.LENGTH_SHORT).show();
        }
        return null;
    }


    private void toggleSignInOutMenu(String signInMenu) {
        MenuItem signOut = mMenu.findItem(R.id.userLogin);
        signOut.setTitle(signInMenu);
    }

    public void createFragment(int fragID) {

        myFragmentManager = getSupportFragmentManager();
        myFragTransaction = myFragmentManager.beginTransaction();

        if (fragID == FB_FRAG_ID) {
            myFragTransaction.add(R.id.frameContainer, new FaceBookFragment(this));
            myFragTransaction.addToBackStack("facebook");
            myFragTransaction.commit();

        } else if (fragID == WHTSAPP_FRAG_ID) {
            myFragTransaction.add(R.id.frameContainer, new WhatsAppFragment());
            myFragTransaction.addToBackStack("whatsapp");
            myFragTransaction.commit();


        } else if (fragID == GPLUS_FRAG_ID) {
            myFragTransaction.add(R.id.frameContainer, new GooglePlusFragment());
            myFragTransaction.addToBackStack("google+");
            myFragTransaction.commit();
        }

    }

    //On Navigation item selected
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        myFragmentManager = getSupportFragmentManager();
        myFragTransaction = myFragmentManager.beginTransaction();

        //getting the ID's.
        int id = item.getItemId();
        final int faceBookID = R.id.menuFacebook;
        final int whatsAppID = R.id.menuWhatsapp;
        final int googlePlusID = R.id.menuGooglePlus;
        final int homeID = R.id.menuHome;
        final int settingsID = R.id.menuSettings;
        final int aboutID = R.id.menuAbout;
        final int helpID = R.id.menuHelp;


        switch (id) {
            case faceBookID:
                myFragTransaction.replace(R.id.frameContainer, new FaceBookFragment(this));
                myFragTransaction.addToBackStack("facebook");
                myFragTransaction.commit();
                break;


            case whatsAppID:
                myFragTransaction.replace(R.id.frameContainer, new WhatsAppFragment());
                myFragTransaction.addToBackStack("whatsapp");
                myFragTransaction.commit();
                break;

            case googlePlusID:
                myFragTransaction.replace(R.id.frameContainer, new GooglePlusFragment());
                myFragTransaction.addToBackStack("google+");
                myFragTransaction.commit();
                break;

            case settingsID:
                myFragTransaction.replace(R.id.frameContainer, new SettingsFragment());
                myFragTransaction.addToBackStack("settings");
                myFragTransaction.commit();
                break;

            case helpID:
                myFragTransaction.replace(R.id.frameContainer, new HelpFragment());
                myFragTransaction.addToBackStack("help");
                myFragTransaction.commit();
                break;

            case aboutID:
                myFragTransaction.replace(R.id.frameContainer, new AboutFragment());
                myFragTransaction.addToBackStack("about");
                myFragTransaction.commit();
                break;


            case homeID:
                int stackCount = myFragmentManager.getBackStackEntryCount();

                if (stackCount == 0) {
                    Toast.makeText(HomeActivity.this, "You are already in HomePage!", Toast.LENGTH_SHORT).show();
                } else {

                    while (stackCount-- != 0) {
                        myFragmentManager.popBackStack();
                    }
                    Toast.makeText(HomeActivity.this, "HomePage!", Toast.LENGTH_SHORT).show();
                }
                break;


        }


        myDrawer.closeDrawer(myNavigation);
        return true;
    }


    //Share status to all FB,WhatsApp,G+.
    public void faceTweet(View view) {
        masterStatus = masterStatusTxt.getText().toString();

        //create seperate thread for posting status on FB,G+,WhatsApp.
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        if (FaceBookCheckBox.isChecked())
                            fbFrag.postToFacebook(masterStatus);

                        if (WhatsAppCheckBox.isChecked())
                            postToWhatsApp(masterStatus);

                        if (GooglePlusCheckBox.isChecked()) {
                                gPlusCheckBoxChecked = true;
                            try {
                                createFragment(GPLUS_FRAG_ID);
                                gPlusFrag = new GooglePlusFragment();
                                gPlusFrag.hStatus = masterStatus;
                                myFragmentManager.popBackStack();

                            } catch (Exception e) {
                                Log.d("g+", "Exception " + e);
                            }

                        }

                        if (FaceBookCheckBox.isChecked() == false && WhatsAppCheckBox.isChecked() == false && GooglePlusCheckBox.isChecked() == false)
                            Toast.makeText(HomeActivity.this, "Please select atleast one item to Share your status with!", Toast.LENGTH_SHORT).show();

                    }
                }, 3000);

        masterStatusTxt.setText(null);
    }

    //Send message to WhatsApp also
    public void postToWhatsApp(String status) {


        PackageManager pm = getPackageManager();
        try {

            final Intent whatsAppIntent = new Intent(Intent.ACTION_SEND);
            whatsAppIntent.setType("text/plain");
            String text = status;

            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            whatsAppIntent.setPackage("com.whatsapp");

            whatsAppIntent.putExtra(Intent.EXTRA_TEXT, text);
            //startActivity(Intent.createChooser(whatsAppIntent, "Share with"));
            startActivityForResult(whatsAppIntent, REQ_WHATSAPP);

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }


        getFragmentManager().popBackStack();
    }


    //Creates the Options menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater mInflatet = getMenuInflater();
        mInflatet.inflate(R.menu.options_menu, menu);

        this.mMenu = menu;

        try {
            if (mLoginPage.isChecked.equals("true"))
                toggleSignInOutMenu("SignOut");
        } catch (NullPointerException e) {
            Toast.makeText(HomeActivity.this, "Exception: " + e, Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    //On Click of Options menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        //Getting the ID's.
        int id = item.getItemId();

        final int loginID = R.id.userLogin;
        final int registerID = R.id.userRegister;
        final int exitID = R.id.exit;


        //Options Menu item selection.
        switch (id) {

            case loginID: {

                try {
                    Log.d("aaa", "in onOptionsItemSelected Home isChecked = " + mLoginPage.isChecked);

                    if (mLoginPage.isChecked.equals("true")) {
                        Log.d("aaa", "in if onOptionsItemSelected Home isChecked = " + mLoginPage.isChecked);
                        Toast.makeText(HomeActivity.this, "You have successfully signed out!", Toast.LENGTH_SHORT).show();

                        toggleSignInOutMenu("Sign In");
                        mLoginPage.isChecked = "false";

                        //clear the userName and Profile picture of user after signing out
                        mCircleProfile.setImageResource(R.drawable.demo_user);
                        profEmail.setText("demo email");
                        profUser.setText("demo user");


                        //set RememberMe CheckBox to false in SharedPref
                        getSharedPreferences(new LoginPage().PREFS_NAME, MODE_PRIVATE)
                                .edit()
                                .putString(new LoginPage().REM_FUNC_CALLED, "false")
                                .commit();
                        userSignedOut = true;
                        onStartCalled = 0;

                        new GooglePlusFragment().isHLogOut = false;

//                        if(gPlusFrag.gLoginResult == true)
//                            gPlusFrag.gLoginResult = false;

                        Log.d("zzz", "in MenuSignout Home onStartCalled = " + onStartCalled);
                        Log.d("TAG", "in Home ,MenuSignout  gLoginResult = " + gPlusFrag.gLoginResult);

                        startActivity(new Intent(this, LoginPage.class));
                        finish();

                    } else {
                        Log.d("aaa", "in else onOptionsItemSelected Home isChecked = " + mLoginPage.isChecked);
                        onStartCalled = 0;
                        Log.d("zzz", "in onOptionsMenu Home onStartCalled = " + onStartCalled);


                        new GooglePlusFragment().isHLogOut = false;

//                        if(gPlusFrag.gLoginResult == true)
//                            gPlusFrag.gLoginResult = false;

                        Log.d("TAG", "in MenuLogin isHLogOut = " + new GooglePlusFragment().isHLogOut);
                        Log.d("TAG", "in Home ,MenuLogin gLoginResult = " + gPlusFrag.gLoginResult);

                        startActivity(new Intent(this, LoginPage.class));
                        finish();
                    }
                } catch (Exception e) {
                    Log.d("aaa", "in excoption onOptionsItemSelected Home isChecked = " + mLoginPage.isChecked);
                    Log.d("aaa", "In SignInOut Exception = " + e);
                }


                //Logout From FaceBook and GooglePlus also...
                try {

                    createFragment(GPLUS_FRAG_ID);
                    gPlusFrag = new GooglePlusFragment();

                    Log.d("TAG", "in Home , //Logout From FaceBook and GooglePlus also. gLoginResult = " + gPlusFrag.gLoginResult);
                    if (gPlusFrag.gLoginResult) {

                        // HSignedOut = true;
                        gPlusFrag.isHLogOut = true;

                        Log.d("TAG", "in //Logout From FaceBook and GooglePlus also... isHLogOut = " + gPlusFrag.isHLogOut);
                    }
                    getFragmentManager().popBackStack();

                    //FaceBookFragment fb = new FaceBookFragment(this);
                    if (fbFrag.logOutFB()) {
                        Toast.makeText(this, "LoggedOut from Facebook Successfully", Toast.LENGTH_SHORT).show();
                    }


                } catch (NullPointerException ex) {
                    Log.d("fb", "fb logout exception = " + ex);
                }

                break;
            }

            case registerID:
                startActivity(new Intent(this, RegisterPage.class));
                break;

            case exitID:
                finishAffinity();
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_WHATSAPP && resultCode == RESULT_OK) {
            Toast.makeText(HomeActivity.this, "Message Sent Successfully!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    //
    @Override
    public void onBackPressed() {
        int stackCount = myFragmentManager.getBackStackEntryCount();

        if (stackCount == 0) {
            doAlert("Exit ?", "Do you want to Exit the App?");
            return;
        } else
            myFragmentManager.popBackStack();
    }

    public void doAlert(String Title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(HomeActivity.this, "Exiting app...", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        onDestroyCalled = true;

        Log.d("TAG", "onDestroy is called");

        //Logout From FaceBook and GooglePlus also...
        try {

            //createFragment(GPLUS_FRAG_ID);
            gPlusFrag = new GooglePlusFragment();

            Log.d("TAG", "in onDestroy gLoginResult = " + gPlusFrag.gLoginResult);
            if (gPlusFrag.gLoginResult) {
                gPlusFrag.isHLogOut = true;
                //createFragment(GPLUS_FRAG_ID);
                myFragTransaction.add(R.id.frameContainer, new GooglePlusFragment());
                Log.d("TAG", "in onDestroy isHLogOut = " + gPlusFrag.isHLogOut);
            }

            //FaceBookFragment fb = new FaceBookFragment(this);
            if (fbFrag.logOutFB())
                Toast.makeText(this, "LoggedOut from Facebook Successfully", Toast.LENGTH_SHORT).show();

        } catch (NullPointerException ex) {
            Log.d("fb", "fb logout exception = " + ex);
        }

    }

    public void reUploadDP(View view) {

//        try {
////            startActivity(new Intent(getApplicationContext(), RegisterPage.class));
////            finish();
//            mRegister.showChooser();
//        }catch (NullPointerException ex){
//            Log.d("aaa",""+ex);
//        }
    }


}
