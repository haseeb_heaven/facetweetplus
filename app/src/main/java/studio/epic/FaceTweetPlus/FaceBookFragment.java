package studio.epic.FaceTweetPlus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.cast.framework.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;


public class FaceBookFragment extends Fragment {

    private int i = 0, k = 0;
    private TextView nameLabel, emailLabel;
    private Button statusButton, timeLineButton;
    private TextView fbUserNameTxt, fbEmailTxt;
    private CallbackManager mCallBackManager;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    protected static String fbUserName, fbEmail, fbUserID, instancefbUser, instancefbEmail;
    boolean postSuccess = false;
    private ListView timeLineListView;
    private String total_count_likes = null, total_count_comments = null;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    LoginButton loginBtn;
    private static ImageView fbUserProfileImage;
    private static Bitmap fbBitmap;
    protected static int fbImageWidth = 320, fbImageHeight = 240;
    private static int index = 0;
    private JSONArray data = null;
    private JSONObject responseObject;
    private JSONObject dataJSONObject;

    protected ArrayList<String> userStatus = new ArrayList<>();
    protected ArrayList<String> userStatusID = new ArrayList<>();
    protected ArrayList<String> userStatusLikes = new ArrayList<>();
    protected ArrayList<String> userStatusComments = new ArrayList<>();


    public FaceBookFragment() {
        // Required empty public constructor
    }

    public FaceBookFragment(Context mActivity) {
        mContext = mActivity;
    }


    private FacebookCallback<LoginResult> mResult = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {


            //set permissions of email, and post status.
            LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email", "user_status", "user_posts"));
            LoginManager.getInstance().logInWithPublishPermissions(getActivity(), Arrays.asList("publish_actions"));


            Toast.makeText(thisActivity(), "Successfully Logged in to Facebook", Toast.LENGTH_SHORT).show();


            // getting username and email from Graph API in JSON form.
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {

                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.d("FB", response.toString());

                            // Application code
                            try {
                                fbUserName = object.getString("name");
                                fbEmail = object.getString("email");
                                fbUserID = object.getString("id");

                                instancefbUser = fbUserName;
                                instancefbEmail = fbEmail;

                                getProfilePicture(fbUserID);
                                showWelcome();


                            } catch (JSONException e) {
                                Log.d("FB", "FB JSONException = " + e);
                            }

                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "name,email,id");
            request.setParameters(parameters);
            request.executeAsync();
        }


        @Override
        public void onCancel() {
            Toast.makeText(thisActivity(), "You Canceled the Login!", Toast.LENGTH_SHORT).show();
            Log.d("FB", "in onCancel Behaviour = " + loginBtn.getLoginBehavior());
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(thisActivity(), "Error  in Login to facebook!", Toast.LENGTH_SHORT).show();
            Log.d("FB", "in onError Behaviour = " + loginBtn.getLoginBehavior());
        }
    };


    public void getProfilePicture(final String id) {

        class fetchDPTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... params) {

                URL image_value = null;
                try {
                    image_value = new URL("https://graph.facebook.com/" + id + "/picture?width=" + fbImageWidth + "&height=" + fbImageHeight);
                    fbBitmap = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());

                    Log.d("FB", "fbBitmap = " + fbBitmap);
                } catch (MalformedURLException e) {
                    Log.d("FB", "Exception indoInBackground " + e);
                } catch (IOException e) {
                    Log.d("FB", "Exception indoInBackground " + e);
                } catch (Exception e) {
                    Log.d("FB", "Exception indoInBackground " + e);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                fbUserProfileImage.setImageBitmap(fbBitmap);
            }

        }

        new fetchDPTask().execute();
    }


    protected void changeProfileResolution(int newWidth, int newHeight) {

        fbImageWidth = newWidth;
        fbImageHeight = newHeight;

    }

    public void showWelcome() {

        if (fbUserName != null && fbEmail != null) {
            nameLabel.setVisibility(View.VISIBLE);
            emailLabel.setVisibility(View.VISIBLE);
            fbUserNameTxt.setText(fbUserName);
            fbEmailTxt.setText(fbEmail);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        mCallBackManager = CallbackManager.Factory.create();


        //your Background thread code here
        mProgressDialog = new ProgressDialog(getActivity());

        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //showWelcome(newProfile);
            }
        };

        //Start Tracking change in Profile and Token
        tokenTracker.startTracking();
        profileTracker.startTracking();

    }

    @Override
    public void onStart() {
        super.onStart();

        if (isLoggedIn()) {

            //setting name and email label invisible.
            nameLabel.setVisibility(View.VISIBLE);
            emailLabel.setVisibility(View.VISIBLE);

            fbUserNameTxt.setText(instancefbUser);
            fbEmailTxt.setText(instancefbEmail);
            fbUserProfileImage.setImageBitmap(fbBitmap);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facebook, null);
        fbUserNameTxt = (TextView) view.findViewById(R.id.fbName);
        fbEmailTxt = (TextView) view.findViewById(R.id.fbEmail);
        statusButton = (Button) view.findViewById(R.id.postStatusButton);
        timeLineButton = (Button) view.findViewById(R.id.readTimelineBtn);
        fbUserProfileImage = (ImageView) view.findViewById(R.id.fbProfileImage);
        timeLineListView = (ListView) view.findViewById(R.id.timeLine);
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        emailLabel = (TextView) view.findViewById(R.id.emailLabel);

        nameLabel.setVisibility(View.INVISIBLE);
        emailLabel.setVisibility(View.INVISIBLE);

        final ShareDialog shareDialog = new ShareDialog(this);


        //post via Webview Activity.
        CallbackManager callbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(callbackManager, new

                FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(getActivity().getApplicationContext(), "Status Updated Successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(thisActivity(), "You Canceled the Status", Toast.LENGTH_SHORT).show();
                    }


                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error posting status", Toast.LENGTH_SHORT).show();
                    }
                });


        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isLoggedIn()) {

                    //if its null only then get the text

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Test Message")
                                .setContentDescription("Posted via FaceTweet app on Android")
                                .build();

                        shareDialog.show(linkContent);

                    }
                } else
                    Toast.makeText(thisActivity(), "Please Sign in to Facebook first!", Toast.LENGTH_SHORT).show();
            }
        });


        timeLineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoggedIn())
                    readTimeLine();
                else
                    Toast.makeText(getActivity(), "Please Login First", Toast.LENGTH_SHORT).show();
            }
        });

        return view;

    }

    //post via FB App.
    public boolean postToFacebook(String status) {

        Bundle parameterss = new Bundle();
        parameterss.putString("message", status);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();


        GraphRequest.Callback checkincallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                FacebookRequestError error = response.getError();
                if (error != null) {
                    Log.d("FB", "onCompleted GraphResponse = " + response.getError().toString());
                    Toast.makeText(mContext, "Error in posting Status to FaceBook! Check if you are Logged in to Facebook", Toast.LENGTH_SHORT).show();
                    postSuccess = false;
                } else {
                    Toast.makeText(mContext, "Successfully posted Status to FaceBook!", Toast.LENGTH_SHORT).show();
                    postSuccess = false;
                }
            }

        };

        GraphRequest request = new GraphRequest(accessToken, "me/feed", parameterss,
                HttpMethod.POST, checkincallback);

        GraphRequestAsyncTask task = new GraphRequestAsyncTask(request);
        task.execute();

        return postSuccess;
    }

    private void readTimeLine() {
        /* make the API call */
        Log.d("FB", "readUserWall called");

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        Log.d("FB", "readUserWall onCompleted called");

                        responseObject = response.getJSONObject();
                        if (response != null) {
                            Log.d("FB", "readUserWall response != null called");

                            //create seperate thread for Background Progress Dialog.
                            fetchInBackground();
                        }
                    }

                }
        ).executeAsync();
    }

    private void fetchInBackground() {

        class fetchTimeLineTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                mProgressDialog.setTitle("TimeLine");
                mProgressDialog.setMessage("Loading timeline please wait...");
                mProgressDialog.show();
            }


            @Override
            protected Void doInBackground(Void... params) {
                Log.d("ADPTR", "doInBackground called");
                try {
                    data = responseObject.getJSONArray("data");

                    for (int i = 0; i <= 30; i++) {
                        dataJSONObject = data.getJSONObject(i);

                        synchronized (getActivity()) {

                            Thread timeLineThread = new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    try {
                                        userStatus.add(index, dataJSONObject.getString("message").toString());
                                        userStatusID.add(index, dataJSONObject.getString("id").toString());

                                        Log.d("ADPTR", "userStatus.add index =  " + index);
                                    } catch (JSONException e) {
                                        Log.d("ADPTR", "read User Wall Exception " + e);
                                    } catch (IndexOutOfBoundsException ex) {
                                        Log.d("ADPTR", "read User Wall Exception " + ex);
                                        Log.d("ADPTR", "timeLineThread IndexOutOfBoundsException index =  " + index);
                                    }
                                }

                            };//Thread


                            //seperate thread for reading likes and comments.
                            Thread fetchThread = new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    try {
                                        Thread.sleep(500);
                                        fetchLikesAndComments(userStatusID.get(index));
                                        index++;
                                    } catch (IndexOutOfBoundsException ex) {
                                        Log.d("ADPTR", "fetchThread IndexOutOfBoundsException index =  " + index);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }

                            };//Thread

                            //Starts Thread
                            timeLineThread.start();
                            timeLineThread.join();

                            //fetchThread.wait();
                            fetchThread.start();
                            fetchThread.join();
                        }
                    }

                } catch (JSONException e) {
                    Log.d("FB", "read User Wall Exception " + e);
                } catch (NullPointerException ex) {
                    Log.d("FB", "read User Wall Exception " + ex);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Log.d("ADPTR", "doInBackground ended");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                Log.d("ADPTR", "timeLineAdapter called");
                timeLineAdapter adapter = new timeLineAdapter(getActivity(), userStatus, userStatusLikes, userStatusComments);
                timeLineListView.setAdapter(adapter);
                new timeLineHeight().setListViewHeightBasedOnChildren(timeLineListView);

                //stops the loading bar on task complete
                mProgressDialog.setMessage("Finished Loading Timeline...");
                mProgressDialog.setProgress(100);
                mProgressDialog.dismiss();
            }

        }
        new fetchTimeLineTask().execute();
    }


    private void fetchLikesAndComments(String postID) {

        Log.d("ADPTR", "fetchLikesAndComments called");

        String access_token = AccessToken.getCurrentAccessToken().getToken();
        final String URL = "https://graph.facebook.com/" + postID + "?fields=likes.summary(true),comments.summary(true),shares&access_token=" + access_token;
        Log.d("URL", "" + URL);

        //Creating Request Queue for Volley.
        RequestQueue reqQue = null;
        reqQue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("ADPTR", "fetchLikesAndComments volley response: " + response);

                        JSONObject responseObject = null;
                        if (response != null) {

                            Log.d("ADPTR", "fetchLikesAndComments response != null");

                            try {
                                //get Likes from Response JSON.
                                responseObject = response.getJSONObject("likes");
                                JSONObject summaryObjectLikes = responseObject.getJSONObject("summary");
                                total_count_likes = summaryObjectLikes.getString("total_count");
                                userStatusLikes.add(k, total_count_likes);

                                //get Comments from Response JSON.
                                responseObject = response.getJSONObject("comments");
                                JSONObject summaryObjectComments = responseObject.getJSONObject("summary");
                                total_count_comments = summaryObjectComments.getString("total_count");
                                userStatusComments.add(k++, total_count_comments);

                                Log.d("ADPTR", "total_count_comments = " + total_count_comments + " total_count_likes =" + total_count_likes + " at index = " + index);

                            } catch (JSONException e) {
                                Log.d("FB", "volley Comments Exception " + e);
                            } catch (NullPointerException ex) {
                                Log.d("FB", "volley Comments Exception " + ex);
                            }
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("FB", "Volley Comments Error: " + error);
                    }
                });

        reqQue.add(strRequest);

    }


//    protected void fetchTimeLineLikes(String PAGE_ID) {
//
//        String access_token = AccessToken.getCurrentAccessToken().getToken();
//        final String likesURL = "https://graph.facebook.com/" + PAGE_ID + "/likes?summary=true&access_token=" + access_token;
//
//        Log.d("URL", "" + likesURL);
//
//        //Creating Request Queue for Volley.
//        RequestQueue reqQue = null;
//        reqQue = Volley.newRequestQueue(getActivity());
//
//        JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.GET, likesURL,
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d("FB", "volley response: " + response);
//
//                        JSONObject responseObject = null;
//                        if (response != null) {
//
//                            try {
//                                responseObject = response.getJSONObject("summary");
//                                total_count_likes = responseObject.getString("total_count");
//
//                                userStatusLikes.add(i++, total_count_likes);
//                                Log.d("ADPTR", "total_count_likes in volley = " + total_count_likes + " index = " + index);
//
//                            } catch (JSONException e) {
//                                Log.d("FB", "volley likes Exception " + e);
//                            } catch (NullPointerException ex) {
//                                Log.d("FB", "volley likes Exception " + ex);
//                            }
//                        }
//
//                    }
//
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d("FB", "Volley Error: " + error);
//                    }
//                });
//
//        reqQue.add(strRequest);
//
//    }


//    private void fetchTimeLineComments(String postID) {
//
//        String access_token = AccessToken.getCurrentAccessToken().getToken();
//        final String likesURL = "https://graph.facebook.com/" + postID + "/comments?summary=true&access_token=" + access_token;
//
//        Log.d("URL", "" + likesURL);
//
//        //Creating Request Queue for Volley.
//        RequestQueue reqQue = null;
//        reqQue = Volley.newRequestQueue(getActivity());
//
//        JsonObjectRequest strRequest = new JsonObjectRequest(Request.Method.GET, likesURL,
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d("FB", "volley response: " + response);
//
//                        JSONObject responseObject = null;
//                        if (response != null) {
//
//                            try {
//                                responseObject = response.getJSONObject("summary");
//                                total_count_comments = responseObject.getString("total_count");
//
//                                userStatusComments.add(k++,total_count_comments);
//                                Log.d("ADPTR", "total_count_comments in volley comment = " + total_count_comments + " index = " + index);
//
//                            } catch (JSONException e) {
//                                Log.d("FB", "volley Comments Exception " + e);
//                            } catch (NullPointerException ex) {
//                                Log.d("FB", "volley Comments Exception " + ex);
//                            }
//                        }
//
//                    }
//
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d("FB", "Volley Comments Error: " + error);
//                    }
//                });
//
//        reqQue.add(strRequest);
//    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginBtn = (LoginButton) view.findViewById(R.id.login_button);
        loginBtn.setFragment(this);
        loginBtn.setTextSize(14.0f);
        loginBtn.registerCallback(mCallBackManager, mResult);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallBackManager.onActivityResult(requestCode, resultCode, data);

    }


    public static boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    protected boolean logOutFB() {

        if (isLoggedIn()) {
            LoginManager.getInstance().logOut();

            try {
                //set Demo content on Logout.
                fbUserNameTxt.setText("");
                fbEmailTxt.setText("");
                fbUserProfileImage.setImageResource(R.drawable.demo_user);
            } catch (NullPointerException ex) {
                Log.d("FB", "Set Default values back " + ex);
            }

            return true;
        } else
            return false;
    }

    public Context thisActivity() {
        return getActivity().getApplicationContext();
    }


    //Stops the tracking
    @Override
    public void onStop() {
        super.onStop();
        tokenTracker.stopTracking();
        profileTracker.stopTracking();
    }
}
