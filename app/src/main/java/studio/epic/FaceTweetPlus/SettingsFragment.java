package studio.epic.FaceTweetPlus;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {

    private static final String PREFS_NAME = "myPref";
    private static final String PREF_PROF_IMG_SIZE = "ProfileImgSizePref";
    private static final int MODE_PRIVATE = 0;


    //constants for Image Quality for FaceTweet
    protected final int LOW_QUALITY = 30;
    protected final int MEDIUM_QUALITY = 50;
    protected final int HIGH_QUALITY = 100;

    private int WIDTH = 0;
    private int HEIGHT = 1;

    protected ArrayList<Integer> FB_LOW_RES = new ArrayList<>();
    protected ArrayList<Integer> FB_MED_RES = new ArrayList<>();
    protected ArrayList<Integer> FB_HIGH_RES = new ArrayList<>();

    private Switch switchProfilePath;
    private SeekBar seekBarfaceTweetImage,seekBarfaceBooktImage;
    private EditText pathTxt;
    private FaceBookFragment mFaceBookFrag;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFaceBookFrag = new FaceBookFragment(getActivity().getApplicationContext());
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        //setting Image Resolutuon Quality of FaceBook Image.

        //Low Resolution 96x96
        FB_LOW_RES.add(WIDTH,96);
        FB_LOW_RES.add(HEIGHT,96);

        //Medium Resolution 128x128
        FB_MED_RES.add(WIDTH,128);
        FB_MED_RES.add(HEIGHT,128);

        //High Resolution 320x240
        FB_HIGH_RES.add(WIDTH,320);
        FB_HIGH_RES.add(HEIGHT,240);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, null);

        switchProfilePath = (Switch) view.findViewById(R.id.switchProfilePath);
        pathTxt = (EditText) view.findViewById(R.id.pathTxt);
        seekBarfaceTweetImage = (SeekBar) view.findViewById(R.id.seekBarrFaceTweetImage);
        seekBarfaceBooktImage = (SeekBar) view.findViewById(R.id.seekBarFaceBookImage);

        pathTxt.setVisibility(View.INVISIBLE);

        //Making Profle_Path Switch clickable.
        switchProfilePath.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    pathTxt.setText("Path: " + new HomeActivity().imgPathFromPref);
                    pathTxt.setVisibility(View.VISIBLE);
                }

                if (!isChecked)
                    pathTxt.setText("Path:/");
            }


        });

        //Making FaceTweet User Profile seekbar clickable.
        seekBarfaceTweetImage.setKeyProgressIncrement(50);
        seekBarfaceTweetImage.setMax(100);
        seekBarfaceTweetImage.setProgress(50);
        seekBarfaceTweetImage.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress >= 0 && progress < 40) {
                    seekBar.setProgress(0);
                    setProfImageSize(LOW_QUALITY);
                    Toast.makeText(getActivity(), "LOW_QUALITY set! changes will be applied after registering of a new user", Toast.LENGTH_SHORT).show();
                }

                if (progress > 40 && progress <= 70) {
                    seekBar.setProgress(50);
                    setProfImageSize(MEDIUM_QUALITY);
                    Toast.makeText(getActivity(), "MEDIUM_QUALITY set! changes will be applied after registering of  anew user", Toast.LENGTH_SHORT).show();
                }

                if (progress > 70 && progress <= 100) {
                    seekBar.setProgress(100);
                    setProfImageSize(HIGH_QUALITY);
                    Toast.makeText(getActivity(), "HIGH_QUALITY set! changes will be applied after registering of a new user", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //   Toast.makeText(getActivity(), "onStartTrackingTouch seekBar ="+seekBar, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Toast.makeText(getActivity(), "onStopTrackingTouch seekBar ="+seekBar, Toast.LENGTH_SHORT).show();
            }
        });


        //Making FaceBook User Profile Image seekbar clickable.
        seekBarfaceBooktImage.setKeyProgressIncrement(50);
        seekBarfaceBooktImage.setMax(100);
        seekBarfaceBooktImage.setProgress(50);
        seekBarfaceBooktImage.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress >= 0 && progress < 40) {
                    seekBar.setProgress(0);
                    setFBImageResolution(FB_LOW_RES.get(WIDTH),FB_LOW_RES.get(HEIGHT));
                    Toast.makeText(getActivity(), "LOW_QUALITY set! changes will be applied after new Login!", Toast.LENGTH_SHORT).show();
                }

                if (progress > 40 && progress <= 70) {
                    seekBar.setProgress(50);
                    setFBImageResolution(FB_MED_RES.get(WIDTH),FB_MED_RES.get(HEIGHT));
                    Toast.makeText(getActivity(), "MEDIUM_QUALITY set! changes will be applied after new Login!", Toast.LENGTH_SHORT).show();
                }

                if (progress > 70 && progress <= 100) {
                    seekBar.setProgress(100);
                    setFBImageResolution(FB_HIGH_RES.get(WIDTH),FB_HIGH_RES.get(HEIGHT));
                    Toast.makeText(getActivity(), "HIGH_QUALITY set! changes will be applied after new Login!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        return view;
    }


    private void setFBImageResolution(int width,int height){
        mFaceBookFrag.fbImageWidth = width;
        mFaceBookFrag.fbImageHeight = height;
    }

    protected void setProfImageSize(int size){
        getActivity().getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .edit()
                .putInt(PREF_PROF_IMG_SIZE,size)
                .commit();
    }

}
