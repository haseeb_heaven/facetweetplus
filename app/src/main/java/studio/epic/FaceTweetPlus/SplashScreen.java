package studio.epic.FaceTweetPlus;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;


public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_splash_screen);
        }catch (OutOfMemoryError e){
            Toast.makeText(SplashScreen.this, ""+e, Toast.LENGTH_SHORT).show();
        }
        finally {
            setContentView(R.layout.activity_splash_screen);
        }


        Thread splashThread = new Thread(){
            @Override
            public void run() {
                super.run();
                try {
                    Thread.sleep(4000); //Sleeps for 4 Seconds
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                finally {
                    startActivity(new Intent(getApplicationContext(),LoginPage.class));
                    finish();
                }

            }

        };//Thread

        //Starts Thread
        splashThread.start();
    }

}